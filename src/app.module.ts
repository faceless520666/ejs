import { Module } from '@nestjs/common';

import { CorsMiddleware } from '@/bing';

import modules from './app';

@Module({
    imports: [...modules]
})

export class ApplicationModule{}