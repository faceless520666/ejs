import { Controller, Get, Param, Post, Body, Request, Response, UseInterceptors, FileInterceptor, UploadedFile } from '@nestjs/common';
import { ApiOperation, ApiUseTags } from '@nestjs/swagger';
import { EJSService } from './ejs.service';
import { TransferDto, BaseDto, UploadDto } from './dto'
import { uploadConfig } from '@/bing/common/uploadConfig'

@ApiUseTags('ejs')
@Controller("ejs")
export class EjsController {

    protected a = {}
    constructor(private readonly ejsService: EJSService) {}

    @Get("/")
    public async index(@Request() req, @Response() res): Promise<any>{
        res.render('users', { title: 'users', name: "Tom" });
    }

    @Get("/upload")
    public async upload(@Request() req, @Response() res): Promise<any>{
        res.render('upload');
    }

    @Post("transfer")
    @ApiOperation({ title: 'get balance from address'})
    public async users(@Body() dto: TransferDto): Promise<any>{
        return this.ejsService.transfer(dto)
    }

    @Post("mulUpload")
    @UseInterceptors(FileInterceptor('photos', uploadConfig))
    public async mulUpload(@Body() dto: UploadDto, @UploadedFile() file): Promise<any>{
        console.log(file)
        return dto
    }
}