import { Module } from '@nestjs/common';
import { EjsController } from './ejs.controller';
import { EJSService } from './ejs.service';

@Module({
  imports: [],
  controllers: [EjsController],
  providers: [EJSService],
})

export class EJSModule {
}